class AddTexteditorFormatsToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :content_delta, :text
    add_column :notes, :content_html, :text
  end
end
