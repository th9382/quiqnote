class AddOwnerIdToTags < ActiveRecord::Migration
  def change
    add_column :tags, :owner_id, :integer, null: false
    add_index :tags, :owner_id
  end
end
