class AddContentHtmlToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :content_html, :text
    add_column :notes, :shared_token, :string

    add_index :notes, :shared_token, unique: true
  end
end
