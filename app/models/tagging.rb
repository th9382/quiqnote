# == Schema Information
#
# Table name: taggings
#
#  id         :integer          not null, primary key
#  note_id    :integer          not null
#  tag_id     :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tagging < ActiveRecord::Base
  after_save :update_counter_cache
  after_destroy :update_counter_cache

  belongs_to :note
  belongs_to :tag
  has_one :user, through: :note, source: :owner

  validates :note_id, :tag_id, presence: true

  private

  def update_counter_cache
    self.tag.active_notes_count = Tagging.joins(:note)
                                         .where("notes.deleted = false")
                                         .where("taggings.tag_id = ?", self.tag.id)
                                         .count
    self.tag.save
  end
end
