QuiqNote.Views.NotesSearch = Backbone.View.extend({
  template: JST['notes/search'],

  events: {
    "keyup #search-query": "search",
    "click .help-button": "walkthrough"
  },

  initialize: function() {
  },

  render: function() {
    var content = this.template();
    this.$el.html(content);
    return this;
  },

  search: function(event) {
    event.preventDefault();
    if(event.keyCode != 13) {
      return;
    }

    var searchString = $("#search-query").val();
    $.ajax({
      url: "/api/notes/search",
      dataType: "json",
      method: "GET",
      data: { query: searchString, page: 1 },
      success: function(response) {
        this.renderSearchResults(response);
      }.bind(this)
    });
  },

  renderSearchResults: function(response) {
    var notes = new QuiqNote.Collections.Notes();

    notes.set(response, { parse: true });

    var notesView = new QuiqNote.Views.SearchNotesIndex({
      collection: notes
    });

    this._currentView && this._currentView.remove();

    this._currentView = notesView;

    this.$('.search-results').html(notesView.render().$el);
  },

  walkthrough: function() {
    $('#main').scrollTop(0);
    var helpGuide = introJs();
    helpGuide.setOptions({
      steps: [
          {
            intro: 'This page allows you to search for all your notes.<br><br>It will find notes by title, content, and tags.',
            position: 'top'
          },
          {
            element: ".search__input",
            intro: "Just type your search string here and press 'enter' to see the results.",
            position: 'bottom'
          }
        ]
    });
    helpGuide.start();
  },
});
