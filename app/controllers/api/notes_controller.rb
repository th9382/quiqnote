class Api::NotesController < ApplicationController
  before_action :require_login

  def index
    if params[:pageType] == 'trash'
      @notes = notes_in_trash
    elsif params[:pageType] == 'shortcuts'
      @notes = notes_with_shortcuts
    elsif params[:pageType] == 'notebook'
      @notes = notes_from_notebook(params[:notebook_id])
    elsif params[:pageType] == 'tags'
      @notes = notes_for_tag(params[:tag_id])
    else
      @notes = current_user.notes.order('updated_at DESC').includes(:tags)
    end

    @notes_count = @notes.count
    if params[:page]
      @notes = @notes.page(params[:page])
      @page = params[:page]
      @total_pages = @notes.total_pages
    end

    render :index
  end

  def show
    @note = Note.includes(:tags).find_by(id: params[:id])
    if @note
      @tags = @note.tags_string
      render :show
    else
      render json: 'invalid note', status: 422
    end
  end

  def create
    @note = Note.new(note_params)
    if @note.save
      @note.set_tags(params[:tags], current_user) if params[:tags]
      render json: @note
    else
      render json: @note.errors.full_messages, status: 422
    end
  end

  def update
    @note = Note.find(params[:id])
    if @note.update(note_params)
      @note.set_tags(params[:tags], current_user) if params[:tags]
      @note.reset_shared_token if params[:resetSharedToken] == true
      render json: @note
    else
      render json: @note.errors.full_messages, status: 422
    end
  end

  def destroy
    @note = Note.find(params[:id])

    if @note.destroy
      render json: @note
    else
      render json: 'invalid note', status: 422
    end
  end

  def search
    if params[:query].present? && params[:query] != ""
      all_notes = current_user.notes
        .joins("left outer join taggings ON taggings.note_id = notes.id ")
        .joins("left outer join tags ON taggings.tag_id = tags.id ")
        .where("notes.title ~* :q OR notes.content ~* :q OR tags.label ~* :q", q: params[:query])
        .uniq
      @notes = all_notes.page(params[:page])
      @notes_count = all_notes.count
      @total_pages = @notes.total_pages
    else
      @notes = Note.none
      @total_pages = 0
    end

    @page = params[:page] || 1
    render :index
  end

  private

  def note_params
    params.require(:note).permit(
      :title,
      :content,
      :content_html,
      :content_delta,
      :shared,
      :deleted,
      :shortcut,
      :notebook_id,
      :tags)
  end

  def notes_in_trash
    current_user.deleted_notes
                .order('updated_at DESC')
                .includes(:tags)
  end

  def notes_with_shortcuts
    current_user.shortcut_notes
                .order('updated_at DESC')
                .includes(:tags)
  end

  def notes_from_notebook(id)
    current_user.notes
                .order('updated_at DESC')
                .where(notebook_id: id)
  end

  def notes_for_tag(id)
    current_user.notes.joins(:tags)
                .order('updated_at DESC')
                .where("tags.id = :id", id: id)

  end
end
