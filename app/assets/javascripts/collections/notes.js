QuiqNote.Collections.Notes = Backbone.Collection.extend({
  comparator: function(noteA, noteB) {
    if (noteA.get('updated_at') > noteB.get('updated_at')) return -1;
    if (noteB.get('updated_at') > noteA.get('updated_at')) return 1;
    return 0;
  },

  model: QuiqNote.Models.Note,

  url: '/api/notes',

  getOrFetch: function(id) {
    var notes = this;
    var note = this.get(id) ||
      new QuiqNote.Models.Note({ id: id });
    note.fetch({
      success: function() {
        notes.add(note, { merge: true });
      }
    });
    return note;
  },

  parse: function(response) {
    this.page = response.page;
    this.total_pages = response.total_pages;
    this.notes_count = response.notes_count;
    return response.notes;
  }
});

QuiqNote.Collections.notes = new QuiqNote.Collections.Notes();
