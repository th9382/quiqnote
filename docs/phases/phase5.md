# Phase 5: Search for Notes / Notebooks / Tags

## Rails
### Models

### Controllers
* Api::NotebooksController (add search action)
* Api::NotesController (add search action)

### Views

## Backbone
### Models

### Collections

### Views
* SearchShow (composite view, contains NotesIndex)

## Gems/Libraries
* TBD
