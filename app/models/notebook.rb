# == Schema Information
#
# Table name: notebooks
#
#  id                 :integer          not null, primary key
#  owner_id           :integer          not null
#  title              :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  allow_delete       :boolean          default(TRUE), not null
#  ord                :integer          default(0), not null
#  active_notes_count :integer          default(0)
#

class Notebook < ActiveRecord::Base
  belongs_to :owner, foreign_key: :owner_id, class_name: 'User'
  has_many :notes, -> { active }, class_name: 'Note'
  has_many :deleted_notes, -> { deleted }, class_name: 'Note'
  has_many :shortcut_notes, -> { shortcut }, class_name: 'Note'

  validates :title, :owner_id, :ord, presence: true
  validates :title, uniqueness: { scope: :owner_id }, length: { maximum: 140 }

  def self.create_default_notebook(user)
    default_notebook = user.notebooks.create!(title: 'Default',
                                              allow_delete: false)
    user.update!(default_notebook_id: default_notebook.id)
    Note.create_welcome_note(default_notebook)
    
    return default_notebook
  end
end
