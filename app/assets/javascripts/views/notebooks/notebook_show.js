QuiqNote.Views.NotebookShow = Backbone.CompositeView.extend({
  template: JST['notebooks/show'],

  className: 'notebook__body',

  initialize: function(options) {
    this.listenTo(this.collection, "sync remove", this.render);
    this.listenTo(this.model, "sync add remove", this.render);
    this.notesIndexView = new QuiqNote.Views.NotebookNotesIndex({
      collection: this.collection,
      model: this.model
    });

    this.addSubview('.notebook__notes', this.notesIndexView);
  },

  render: function() {
    this.$el.html(this.template({ notebook: this.model }));
    this.attachSubviews();
    this.$("abbr.timeago").timeago();
    return this;
  }
});
