QuiqNote.Views.ShortcutsNotesIndex = QuiqNote.Views.NotesIndexBase.extend({
  initialize: function() {
    QuiqNote.Views.NotesIndexBase.prototype.initialize.call(this);

    this.pageType = 'shortcuts';
  },

  dataAttribs: function() {
    return {
      page: ++this.collection.page,
      pageType: this.pageType
    };
  },

  walkthrough: function() {
    $('#main').scrollTop(0);
    var helpGuide = introJs();
    helpGuide.setOptions({
      steps: [
          {
            element: '.notes__body',
            intro: 'This page shows you all the notes for which you created a shortcut.',
            position: 'left'
          },
          {
            element: '.notes__list__item:first-child',
            intro: "Like on the other pages, simply click on the note to see all the content.<br><br> If you want to remove the shortcut, just hover over the note and click on the shortcut button.",
            position: 'right'
          }
      ]
    });
    helpGuide.start();
  }
});
