QuiqNote.Models.Notebook = Backbone.Model.extend({
  urlRoot: '/api/notebooks',

  modelType: 'Notebook',
});
