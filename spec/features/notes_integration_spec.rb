require 'rails_helper'

# save_and_open_page

feature "Notes" do
  background do
    @user = User.create!(email: 'example@example.com', password: 'password')
    @notebook = @user.notebooks.create(title: "Example_Notebook", ord: 0)
    @note = @notebook.notes.create(title: "testtest",
      content: "test_note_test_note",
      content_html: "<div>test_note_test_note</div>",
      content_delta: "{\"ops\":[{\"insert\":\"test_note_test_note\\n\"}]}",
      shared: false,
      shared_token: "abcdef")
  end

  before(:each) do
    page.visit "/session/new"
    page.fill_in "user[email]", with: 'example@example.com'
    page.fill_in "user[password]", with: 'password'
    page.click_button "Sign in"
  end

  scenario "Successfully create a new note", :js => true do
    page.visit "/#/notebooks/#{@notebook.id}"
    find('.sidebar__list__item--new-note').click
    fill_in "note_title", with: 'Example_Note';
    click_button "Done"
    visit '/#/notes'
    expect(page).to have_content("Example_Note")
  end

  scenario "Successfully Edit a note", :js => true do
    page.visit "/#/notes/#{ @note.id }"
    page.fill_in "note_title", with: 'edited_title'
    page.click_button "Done"
    page.visit "/#/notes/#{ @note.id }"
    expect(page).to_not have_content('testtest')
    expect(page).to have_content('edited_title')
  end

  scenario "Successfully view note", :js => true do
    page.visit "/#/notes/#{ @note.id }"
    expect(page).to have_selector("input[value='testtest']")
    expect(page).to have_content(@note.content)
  end

  scenario "Show note in index view", :js => true do
    page.visit "/#/notes"
    expect(page).to have_content(@note.content)
    expect(page).to have_content(@note.title)
  end

  scenario "Add shortcut to note", :js => true do
    page.visit "/#/notes"
    find(".actions__img--noshortcut").click
    expect(page).to have_content('Created shortcut')
    page.visit "/#/shortcuts"
    expect(page).to have_content(@note.title)
  end

  scenario "Remove shortcut to note", :js => true do
    page.visit "/#/notes"
    find(".actions__img--noshortcut").click
    find(".actions__img--shortcut").click
    expect(page).to have_content('Removed shortcut')
    page.visit "/#/shortcuts"
    expect(page).to_not have_content(@note.title)
  end

  scenario "Can share notes", :js => true do
    page.visit "/#/notes/#{@note.id}"
    page.click_button "Share"
    find('.button_confirm').click
    page.visit "/shared/abcdef"
    expect(page).to have_selector("input[value='testtest']")
    expect(page).to have_content(@note.content)
  end

  scenario "Can stop sharing notes", :js => true do
    page.visit "/#/notes/#{@note.id}"
    page.click_button "Share"
    page.click_link "Stop sharing note"
    page.visit "/shared/abcdef"
    expect(page).to have_content(@note.title)
  end

  scenario "Permanently erase note", :js => true do
    visit '/#/notes'
    expect(page).to have_content(@note.title)
    find(".actions__img--trash").click
    page.click_button "Delete"
    visit '/#/notes'
    expect(page).to_not have_content(@note.title)
    visit '/#/trash'
    expect(page).to have_content(@note.title)
    page.click_button "Erase"
    page.click_button "Delete"
    visit '/#/trash'
    expect(page).to_not have_content(@note.title)
    visit '/#/notes'
    expect(page).to_not have_content(@note.title)
  end

  scenario "Restore a note", :js => true do
    visit '/#/notes'
    expect(page).to have_content(@note.title)
    find(".actions__img--trash").click
    page.click_button "Delete"
    visit '/#/notes'
    expect(page).to_not have_content(@note.title)
    visit '/#/trash'
    expect(page).to have_content(@note.title)
    page.click_button "Restore"
    visit '/#/trash'
    expect(page).to_not have_content(@note.title)
    visit '/#/notes'
    expect(page).to have_content(@note.title)
  end

  scenario "Search for a note", :js => true do
    visit '/#/notes'
    expect(page).to have_content(@note.title)
    visit '/#/search'
    expect(page).to_not have_content(@note.title)
    page.fill_in "search-query", with: "t\n"
    page.execute_script %{ $('#search-query').trigger('keyup') }
    expect(page).to have_content(@note.title)
  end
end
