class AddNotesCounterCache < ActiveRecord::Migration
  def change
    add_column :notebooks, :notes_count, :integer, default: 0
  end
end
