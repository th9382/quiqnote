QuiqNote.Views.TagShow = Backbone.CompositeView.extend({
  template: JST['tags/show'],

  className: 'notebook__body',

  initialize: function(options) {
    this.listenTo(this.collection, "sync remove", this.render);
    this.listenTo(this.model, "sync add remove", this.render);
    this.notesIndexView = new QuiqNote.Views.TagsNotesIndex({
      collection: this.collection,
      model: this.model
    });

    this.addSubview('.notebook__notes', this.notesIndexView);
  },

  render: function() {
    this.$el.html(this.template({ tag: this.model }));

    this.attachSubviews();
    this.$("abbr.timeago").timeago();
    return this;
  }
});
