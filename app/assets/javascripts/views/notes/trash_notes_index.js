QuiqNote.Views.TrashNotesIndex = QuiqNote.Views.NotesIndexBase.extend({
  initialize: function() {
    QuiqNote.Views.NotesIndexBase.prototype.initialize.call(this);

    this.pageType = 'trash';
  },

  dataAttribs: function() {
    return {
      page: ++this.collection.page,
      pageType: this.pageType
    };
  },

  walkthrough: function() {
    $('#main').scrollTop(0);
    var helpGuide = introJs();
    helpGuide.setOptions({
      steps: [
          {
            element: '.notes__body',
            intro: 'This page shows you all the notes that were moved to the trash.',
            position: 'left'
          },
          {
            element: '.notes__list__item:first-child',
            intro: "If you hover over the note, you get two options:<br><br>1) Restore the note, which will move it back to the notebook it was deleted from (or the default notebook if the other notebook no longer exists).<br><br>2) Erase the note, which permanently deletes it. You will need to confirm this action, so don't worry about clicking this by mistake.",
            position: 'right'
          }
      ]
    });
    helpGuide.start();
  }
});
