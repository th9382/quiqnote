class Api::NotebooksController < ApplicationController
  before_action :require_login

  def index
    @notebooks = current_user.notebooks.order('ord ASC').includes(:notes)
    render :index
  end

  def show
    @notebook = Notebook.includes(:notes).find_by(id: params[:id])
    if @notebook
      @page = 1
      @total_pages = @notebook.notes.page(@page).per(25).total_pages
      @notes_count = @notebook.notes.count
      render :show
    else
      render json: 'invalid notebook', status: 422
    end
  end

  def create
    @notebook = current_user.notebooks.new(notebook_params)
    if @notebook.save
      render json: @notebook
    else
      render json: @notebook.errors.full_messages, status: 422
    end
  end

  def update
    @notebook = Notebook.find(params[:id])
    if @notebook.update(notebook_params)
      render json: @notebook
    else
      render json: @notebook.errors.full_messages, status: 422
    end
  end

  def destroy
    @notebook = Notebook.find(params[:id])
    if @notebook && @notebook.allow_delete
      @notebook.notes.update_all(
        deleted: true,
        notebook_id: current_user.default_notebook_id
      )
      @notebook.destroy
      render json: @notebook
    else
      render json: 'unable to delete notebook', status: 422
    end
  end

  private

  def notebook_params
    params.require(:notebook).permit(:title, :ord)
  end
end
