class AddContentDeltaToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :content_delta, :text
  end
end
