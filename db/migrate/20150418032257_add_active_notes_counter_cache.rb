class AddActiveNotesCounterCache < ActiveRecord::Migration
  def change
    add_column :notebooks, :active_notes_count, :integer, default: 0
  end
end
