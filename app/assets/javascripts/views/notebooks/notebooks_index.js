QuiqNote.Views.NotebooksIndex = Backbone.CompositeView.extend({
  template: JST['notebooks/index'],

  className: 'col-xs-10 col-xs-offset-1 col-lg-6 col-lg-offset-3 notebook__body',

  events: {
    'sortstop': 'saveNewOrds',
    "click .help-button": "walkthrough"
  },

  saveNewOrds: function(event) {
    var $notebooks = this.$('.notebook__list').children('li');

    $notebooks.each(function(index, notebook) {
      var $notebook = $(notebook);
      var notebookId = $notebook.data('id');
      var nbModel = this.collection.get(notebookId);

      if (!!nbModel && nbModel.get('ord') === index) {
        return;
      }
      nbModel && nbModel.save({ord: index}, { renderFalse: true });
    }.bind(this));

    this.collection.sort();
  },

  initialize: function(options) {
    this.listenTo(this.collection, "sync", this.render);
    this.listenTo(this.collection, 'add', this.addIndexItem);
    this.listenTo(this.collection, 'remove', this.removeIndexItem);

    this._subviews = {};

    this.collection.each(this.addIndexItem.bind(this));
  },

  addIndexItem: function (notebook) {
    var subview = new QuiqNote.Views.NotebooksIndexItem({
      model: notebook,
      collection: this.collection,
    });

    if (notebook.escape('title') === 'Default') {
      subview.$el.addClass("not-sortable");
      this.addSubview('.notebook__list', subview, { unshift: true });
    } else {
      this.addSubview('.notebook__list', subview);
    }
  },

  removeIndexItem: function (notebook) {
    this.removeModelSubview('.notebook__list', notebook);
  },

  render: function(model, attr, options) {
    if (options && options.renderFalse) {
      return;
    }
    this.$el.html(this.template());
    this.attachSubviews();

    $( ".notebook__list" ).sortable({items: "> li:not(.not-sortable)"});
    $( ".notebook__list" ).disableSelection();

    return this;
  },

  walkthrough: function() {
    $('#main').scrollTop(0);
    var helpGuide = introJs();
    helpGuide.setOptions({
      steps: [
          {
            element: '.notebook__list',
            intro: 'This page shows all your notebooks.<br><br>You can sort notebooks by dragging them (left-click and hold, then move notebook up or down).<br><br>Clicking on a notebook will show a new page with a list of notes associated with that notebook.',
            position: 'left'
          },
          {
            element: '.notebooks__create',
            intro: "Clicking this will bring you to a page where you can create a new notebook.<br><br>Notebook titles need to be unique, otherwise you will get an error message.",
            position: 'right'
          },
          {
            element: '.notebooks__trash',
            intro: "This will bring you to the trash view, where you can permanently delete any notes that are in the trash or restore them.",
            position: 'right'
          },
          {
            element: '.notebook__list__item:first-child',
            intro: "This is your Default notebook. It can't be deleted or moved.",
            position: 'left'
          },
          {
            element: '.notebook__list__item:nth-child(2)',
            intro: "If you hover over a non-Default notebook, you will be given an option to delete it.<br><br>You will need to confirm the deletion, so don't worry about accidentally clicking this.<br><br>When you delete a notebook, all containing notes will be moved to the trash (i.e. they don't get permanently deleted).",
            position: 'right',
            scrollToElement: true
          }
      ]
    });
    helpGuide.start();
  }
});
