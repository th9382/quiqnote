json.notes @notes do |note|
    json.partial! 'api/notes/note_light', note: note
end

json.set! :page, @page
json.set! :total_pages, @total_pages
json.set! :notes_count, @notes_count
