window.QuiqNote = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  previousView: {},
  compactView: false,
  initialize: function() {
    new QuiqNote.Routers.Router({ $rootEl: $('#content')});
    Backbone.history.start();
  }
};

$(document).ready(function(){
  QuiqNote.initialize();
  });
