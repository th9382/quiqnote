class Api::TagsController < ApplicationController
  before_action :require_login

  def show
    @tag = current_user.tags
                       .includes(:notes)
                       .find_by(id: params[:id])
    if @tag
      @notes_count = @tag.notes.count
      puts @notes_count
      render :show
    else
      render json: 'invalid tag', status: 422
    end
  end

  def index
    @tags = current_user.tags.order("label ASC")
    if @tags
      render :index
    else
      render json: 'invalid tag', status: 422
    end
  end

  def destroy
    tag = current_user.tags.find_by(id: params[:id])

    if tag.destroy
      render json: tag
    else
      render json: 'invalid tag', status: 422
    end
  end

  def search
    if params[:query].present?
      puts params[:query]
      @tags = current_user.tags.where("tags.label ~ :q", q: params[:query]).pluck(:label)
      puts @tags
    else
      @tags = Tag.none
    end
  end
end
