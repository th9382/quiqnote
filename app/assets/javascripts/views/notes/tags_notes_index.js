QuiqNote.Views.TagsNotesIndex = QuiqNote.Views.NotesIndexBase.extend({
  initialize: function() {
    QuiqNote.Views.NotesIndexBase.prototype.initialize.call(this);

    this.pageType = 'tags';
  },

  dataAttribs: function() {
    return {
      page: ++this.collection.page,
      pageType: this.pageType,
      tag_id: this.model.id
    };
  },

  walkthrough: function() {}
});
