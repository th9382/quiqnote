class RemoveContentHtmlFromNotes < ActiveRecord::Migration
  def change
    remove_column :notes, :content_html
  end
end
