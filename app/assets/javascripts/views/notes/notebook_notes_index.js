QuiqNote.Views.NotebookNotesIndex = QuiqNote.Views.NotesIndexBase.extend({
  initialize: function() {
    QuiqNote.Views.NotesIndexBase.prototype.initialize.call(this);

    this.pageType = 'notebook';
    this.className = 'notes__container--notebook';
  },

  dataAttribs: function() {
    return {
      page: ++this.collection.page,
      pageType: this.pageType,
      notebook_id: this.model.id
    };
  },

  walkthrough: function() {}
});
