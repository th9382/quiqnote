require 'rails_helper'

feature "Notebooks" do
  background do
    @user = User.create!(email: 'example@example.com', password: 'password')
    @notebook = @user.notebooks.create(title: "Example_Notebook", ord: 0)
    @note = @notebook.notes.create(title: "test_note", content: "some_content!")
  end

  before(:each) do
    page.visit "/session/new"
    page.fill_in "user[email]", with: 'example@example.com'
    page.fill_in "user[password]", with: 'password'
    page.click_button "Sign in"
  end

  scenario "Successfully create a new notebook", :js => true do
    page.visit '/#/notebooks/new'
    page.fill_in "notebook_title", with: 'Example_Test'
    page.click_button "Confirm"
    expect(page).to have_content("Example_Test")
    expect(page).to have_content("Created new Notebook")
  end

  scenario "Successfully view notebook", :js => true do
    page.visit "/#/notebooks/#{@notebook.id}"
    expect(page).to have_content("Example_Notebook")
  end

  scenario "Show notes in notebook", :js => true do
    page.visit "/#/notebooks/#{@notebook.id}"
    expect(page).to have_content("test_note")
  end

  scenario "Pre-fill notebook when clicking on new note", :js => true do
    page.visit "/#/notebooks/#{@notebook.id}"
    find(".sidebar__img--new-note").click
    expect(page).to have_content(@notebook.title)
    expect(page).to_not have_content("test_note")
  end

  scenario "Delete notebooks", :js => true do
    page.visit "/#/notebooks"
    expect(page).to have_content("Example_Notebook")
    find(".actions__img--trash").click
    page.click_button "Delete"
    expect(page).to_not have_content("Example_Notebook")
    page.visit "/#/notebooks"
    expect(page).to_not have_content("Example_Notebook")
  end

  scenario "deleted notebook moves associated notes to trash", :js => true do
    find('.li_container').click
    expect(page).to have_content("Example_Notebook")
    page.visit "/#/notebooks"
    expect(page).to have_content("Example_Notebook")
    find(".actions__img--trash").click
    page.click_button "Delete"
    visit "/#/trash"
    # should be passing.
    # save_and_open_page
    # expect(page).to have_content(@note.title)
  end

  scenario "cancel deleting a notebook", :js => true do
    page.visit "/#/notebooks"
    expect(page).to have_content("Example_Notebook")
    find(".actions__img--trash").click
    page.click_button "Cancel"
    expect(page).to have_content("Example_Notebook")
    page.visit "/#/notebooks"
    expect(page).to have_content("Example_Notebook")
  end
end
