QuiqNote.Views.NotebookForm = Backbone.View.extend({
  template: JST['notebooks/form'],

  events: {
    "click .notebook--new__button--cancel": "cancel",
    "click .notebook--new__button--confirm": "createNotebook"
  },

  cancel: function(event) {
    event.preventDefault();
    Backbone.history.navigate("/notebooks", { trigger: true });
  },

  createNotebook: function(event) {
    event.preventDefault();
    var attr = { title: $("#notebook_title").val() };
    attr.ord = this.collection.length;
    this.model.save(attr, {
      success: function(model, response) {
        this.collection.add(model);
        humane.log('Created new Notebook', { timeout: 1900, clickToClose: true });
        Backbone.history.navigate("#/notebooks", { trigger: true });
      }.bind(this),
      error: function(model, response) {
        var msg = response.responseText.replace(/\[|\]|"/g, "");
        humane.error = humane.spawn({ addnCls: 'humane-libnotify-error', timeout: 1900 });
        humane.error(msg);
      }
    });
  },

  render: function() {
    var content = this.template({
      notebook: this.model
    });

    this.$el.html(content);
    return this;
  }
});
