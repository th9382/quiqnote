json.array! @tags do |tag|
  json.extract! tag, :id, :label, :active_notes_count
end
