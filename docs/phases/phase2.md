# Phase 2: Viewing and creating Notebooks and Notes

## Rails
### Models
* Notebook
* Note

### Controllers
* Api::NotebooksController (create, destroy, index, show, update)
* Api::NotesController (create, destroy, index, show, update)

### Views
* notebooks/show.json.jbuilder
* notes/show.json.jbuilder

## Backbone
### Models
* Notebook (also parses nested `notes` association)
* Note

### Collections
* Notebooks
* Notes

### Views
* NotebookIndex
* NotebookIndexItem
* NotebookForm
* NotebookShow (composite view, contains NotesIndex subview)
* NotesIndex (composite view, contains NotesIndexItem subviews)
* NotesIndexItem
* NoteShow (this is also the form to allow for seamless editing)

## Gems/Libraries
* backbone-on-rails gem
