QuiqNote.Views.Modal = Backbone.View.extend({
  template: JST['layouts/modal'],
  events: {
    "click button.button_cancel": "cancel",
    "click button.button_confirm": "confirm"
  },
  cancel: function() {
    this.remove();
  },
  confirm: function() {
    if (this.model.modelType === 'Note') {
      if (this.model.get("deleted") === true) {
        this.model.destroy({
          success: function() {
            humane.log('Erased ' + this.model.escape('title'), { timeout: 1900, clickToClose: true });
          }.bind(this)
        });
      } else {
        this.model.save({ deleted: true }, {
          patch: true,
          success: function() {
            humane.log('Moved ' + this.model.escape('title') + ' to trash', { timeout: 1900, clickToClose: true });
          }.bind(this)
        });
      }
    } else {
      // immediately erase notebooks
      this.model.destroy({
        success: function() {
          humane.log('Erased '+ this.model.modelType, { timeout: 1900, clickToClose: true });
        }.bind(this)
      });
    }
    this.collection.remove(this.model, { trigger: true });
    this.remove();
  },
  render: function() {
    var content = this.template({
      dim: this.center(),
      model: this.model
    });
    this.$el.html(content);
    $("body").append(this.$el);
    return this;
  },
  center: function() {
    var top, left;
    top = ($(window).height() - 306) * 0.15;
    left = ($(window).width() - 650) / 2;
    return { top: top, left: left };
  }
});
