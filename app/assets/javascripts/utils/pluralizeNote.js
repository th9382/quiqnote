QuiqNote.pluralizeNote = function(num) {
  if (num === 1) {
    return 'note';
  } else {
    return 'notes';
  }
};
