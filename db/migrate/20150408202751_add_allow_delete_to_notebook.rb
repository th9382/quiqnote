class AddAllowDeleteToNotebook < ActiveRecord::Migration
  def change
    add_column :notebooks, :allow_delete, :boolean, null: false, default: true
  end
end
