QuiqNote.Views.TagsIndexItem = Backbone.View.extend({
  template: JST['tags/index_item'],

  className: 'row notebook__list__item',

  tagName: 'li',

  events: {
    "click .actions__img--trash": "spawnModal"
  },

  render: function() {
    var content = this.template({ tag: this.model });
    this.$el.html(content);
    return this;
  },

  spawnModal: function(event) {
    event.preventDefault();
    var modal = new QuiqNote.Views.Modal({
      model: this.model,
      collection: this.collection
    });
    modal.render();
  }
});
