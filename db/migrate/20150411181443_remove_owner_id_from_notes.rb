class RemoveOwnerIdFromNotes < ActiveRecord::Migration
  def change
    remove_column :notes, :owner_id
  end
end
