# == Schema Information
#
# Table name: notes
#
#  id            :integer          not null, primary key
#  notebook_id   :integer          not null
#  title         :string           not null
#  content       :text
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  content_delta :text
#  deleted       :boolean          default(FALSE)
#  shortcut      :boolean          default(FALSE)
#  content_html  :text
#  shared_token  :string
#  shared        :boolean          default(FALSE)
#

class Note < ActiveRecord::Base
  after_initialize :ensure_shared_token
  after_save :update_counter_cache
  after_destroy :update_counter_cache

  scope :active, -> { where(deleted: false) }
  scope :deleted, -> { where(deleted: true) }
  scope :shortcut, -> { where(shortcut: true, deleted: false) }

  belongs_to :notebook
  has_one :owner, through: :notebook, source: :owner
  has_many :taggings, class_name: 'Tagging', dependent: :destroy
  has_many :tags, through: :taggings, source: :tag

  validates :title, :notebook_id, presence: true
  validates :title, length: { maximum: 140 }
  validates :shared_token, uniqueness: true

  def self.create_welcome_note(notebook)
    notebook.notes.create!({title: "Welcome...", content: "QuiqNote is a note-taking app (inspired by Evernote's web app) that allows users to create rich content (notes), organize those notes, and easily find notes via search, tags, shortcuts, and notebooks. The overall design is minimalistic with rich user feedback when navigating the site.\n\nClick on the 'Explain this page' link on the left to get a quick walk-through of the page you're on.\n\nFeatures include:\nRich text editing. Bold, Italic, Underline, Strikethrough and more.\nYou can include links in your notes: www.quiqnote.com and pictures\nMove notes to trash and restore them or permanently erase them.\nOrganize your notes in Notebooks\nQuickly get to notes by creating a Shortcut\nGroup notes even further with the use of Tags. You can assign many tags to notes.\nShare notes via a unique URL\nSortable notebooks\nInfinite scrolling\nCompact list views\nWalk-throughs\n\nBrief overview:\nCreate a new note by clicking on the + icon in the sidebar.\nThe Notes link will show all your notes\nThe Notebooks link will show all your notebooks. Deleting a notebook will move all the containing notes to the trash. You can restore notes from the trash, which will move it to the notebook it was deleted from, or if that notebook doesn't exist, it will be added to your default notebook.\nThe Shortcuts page will give you a list of all the notes that you created shortcuts for\nThe Tags page will display all tags that you created. You can click into each tag to get a list of all the notes associated with that tag. Deleting a tag will remove the tag association from all your notes, but it will not delete any of your notes.\n\n", content_delta: "{\"ops\":[{\"attributes\":{\"color\":\"rgb(51, 51, 51)\",\"font\":\"'Helvetica Neue', Helvetica, 'Segoe UI', Arial, freesans, sans-serif\"},\"insert\":\"QuiqNote is a note-taking app (inspired by Evernote's web app) that allows users to create rich content (notes), organize those notes, and easily find notes via search, tags, shortcuts, and notebooks. The overall design is minimalistic with rich user feedback when navigating the site.\"},{\"insert\":\"\\n\\n\"},{\"attributes\":{\"bold\":true},\"insert\":\"Click on the '\"},{\"attributes\":{\"bold\":true,\"underline\":true},\"insert\":\"Explain this page'\"},{\"attributes\":{\"bold\":true},\"insert\":\" link on the left to get a quick walk-through of the page you're on.\"},{\"insert\":\"\\n\\n\"},{\"attributes\":{\"size\":\"18px\"},\"insert\":\"Features include:\"},{\"insert\":\"\\nRich text editing. \"},{\"attributes\":{\"bold\":true},\"insert\":\"Bold\"},{\"insert\":\", \"},{\"attributes\":{\"italic\":true},\"insert\":\"Italic\"},{\"insert\":\", \"},{\"attributes\":{\"underline\":true},\"insert\":\"Underline\"},{\"insert\":\", \"},{\"attributes\":{\"strike\":true},\"insert\":\"Strikethrough\"},{\"insert\":\" and more.\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"You can include links in your notes: www.quiqnote.com and pictures\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"Move notes to trash and restore them or permanently erase them.\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"Organize your notes in \"},{\"attributes\":{\"underline\":true},\"insert\":\"Notebooks\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"Quickly get to notes by creating a \"},{\"attributes\":{\"underline\":true},\"insert\":\"Shortcut\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"Group notes even further with the use of \"},{\"attributes\":{\"underline\":true},\"insert\":\"Tags\"},{\"insert\":\". You can assign many tags to notes.\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"Share notes via a unique URL\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"Sortable notebooks\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"Infinite scrolling\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"Compact list views\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"Walk-throughs\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"\\n\"},{\"attributes\":{\"size\":\"18px\"},\"insert\":\"Brief overview:\"},{\"insert\":\"\\nCreate a new note by clicking on the \"},{\"attributes\":{\"bold\":true,\"size\":\"18px\"},\"insert\":\"+\"},{\"insert\":\" icon in the sidebar.\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"The Notes link will show all your notes\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"The Notebooks link will show all your notebooks. Deleting a notebook will move all the containing notes to the trash. You can restore notes from the trash, which will move it to the notebook it was deleted from, or if that notebook doesn't exist, it will be added to your default notebook.\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"The Shortcuts page will give you a list of all the notes that you created shortcuts for\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"The Tags page will display all tags that you created. You can click into each tag to get a list of all the notes associated with that tag. Deleting a tag will remove the tag association from all your notes, but it will not delete any of your notes.\"},{\"attributes\":{\"list\":true},\"insert\":\"\\n\"},{\"insert\":\"\\n\"}]}", deleted: false, shortcut: true, content_html: "<div><span style=\"color: rgb(51, 51, 51); font-family: 'Helvetica Neue', Helvetica, 'Segoe UI', Arial, freesans, sans-serif;\">QuiqNote is a note-taking app (inspired by Evernote's web app) that allows users to create rich content (notes), organize those notes, and easily find notes via search, tags, shortcuts, and notebooks. The overall design is minimalistic with rich user feedback when navigating the site.</span></div><div><br></div><div><b>Click on the '<u>Explain this page'</u> link on the left to get a quick walk-through of the page you're on.</b></div><div><br></div><div><span style=\"font-size: 18px;\">Features include:</span></div><ol><li>Rich text editing. <b>Bold</b>, <i>Italic</i>, <u>Underline</u>, <s>Strikethrough</s> and more.</li><li>You can include links in your notes: www.quiqnote.com and pictures</li><li>Move notes to trash and restore them or permanently erase them.</li><li>Organize your notes in <u>Notebooks</u></li><li>Quickly get to notes by creating a <u>Shortcut</u></li><li>Group notes even further with the use of <u>Tags</u>. You can assign many tags to notes.</li><li>Share notes via a unique URL</li><li>Sortable notebooks</li><li>Infinite scrolling</li><li>Compact list views</li><li>Walk-throughs</li></ol><div><br></div><div><span style=\"font-size: 18px;\">Brief overview:</span></div><ol><li>Create a new note by clicking on the <b style=\"font-size: 18px;\">+</b> icon in the sidebar.</li><li>The Notes link will show all your notes</li><li>The Notebooks link will show all your notebooks. Deleting a notebook will move all the containing notes to the trash. You can restore notes from the trash, which will move it to the notebook it was deleted from, or if that notebook doesn't exist, it will be added to your default notebook.</li><li>The Shortcuts page will give you a list of all the notes that you created shortcuts for</li><li>The Tags page will display all tags that you created. You can click into each tag to get a list of all the notes associated with that tag. Deleting a tag will remove the tag association from all your notes, but it will not delete any of your notes.</li></ol><div><br></div>"})
  end

  def reset_shared_token
    self.shared_token = SecureRandom.urlsafe_base64
    save
    shared_token
  end

  def set_tags(str, user)
    old_tags_labels = tags.pluck(:label)
    new_tags_labels = str.split(',')

    tags_labels_to_destroy = old_tags_labels - new_tags_labels
    tags_labels_to_create = new_tags_labels - old_tags_labels

    ActiveRecord::Base.transaction do
      # create new tags in database
      all_tags = Tag.where(owner_id: user.id).pluck(:label)
      new_db_tags = tags_labels_to_create.reject do |tag|
        all_tags.include?(tag)
      end
      new_db_tags.each do |tag|
        Tag.create!(label: tag, owner_id: user.id)
      end

      # get ids from labels to create taggings
      tag_ids_to_destroy = Tag.where(owner_id: user.id, label: tags_labels_to_destroy)
      tag_ids_to_create = Tag.where(owner_id: user.id, label: tags_labels_to_create)

      # destroy taggings
      Tagging.where(note_id: id, tag_id: tag_ids_to_destroy).destroy_all

      # create taggings
      tag_ids_to_create.each do |tag|
        Tagging.create!(note_id: id, tag_id: tag.id)
      end
    end
  end

  def tags_string
    tags.pluck(:label).join(',')
  end

  private
  def ensure_shared_token
    self.shared_token ||= SecureRandom.urlsafe_base64
  end

  def update_counter_cache
    self.notebook.active_notes_count = Note.where(deleted: false, notebook_id: self.notebook.id).count
    self.notebook.save

    tags.each do |tag|
      tag.update_active_notes_count
    end
  end
end
