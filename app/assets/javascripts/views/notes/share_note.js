QuiqNote.Views.ShareNote = Backbone.View.extend({
  template: JST['notes/share'],
  events: {
    "click a.stop-sharing": "stopSharing",
    "click button.button_confirm": "startSharing",
    "click button.button_cancel": "cancel"
  },
  cancel: function() {
    this.remove();
  },

  stopSharing: function() {
    // set the shared flag to false -> should trigger reset of token
    this.model.save({ shared: false, resetSharedToken: true }, {
      patch: true,
      success: function() {
        humane.log("Stopped sharing the note.", { timeout: 1900, clickToClose: true });
      }.bind(this)
    });

    this.remove();
  },

  startSharing: function() {
    // set the shared flag to true
    this.model.save({ shared: true, resetSharedToken: false}, {
      patch: true,
      success: function() {
        humane.log("Note is now being shared", { timeout: 1900, clickToClose: true });
      }.bind(this)
    });

    this.remove();
  },

  render: function() {
    var content = this.template({
      dim: this.center(),
      note: this.model
    });
    this.$el.html(content);
    $("body").append(this.$el);
    return this;
  },
  
  center: function() {
    var top, left;
    top = ($(window).height() - 306) * 0.15;
    left = ($(window).width() - 650) / 2;
    return { top: top, left: left };
  }
});
