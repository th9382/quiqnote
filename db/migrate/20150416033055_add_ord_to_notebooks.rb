class AddOrdToNotebooks < ActiveRecord::Migration
  def change
    add_column :notebooks, :ord, :integer, null: false, default: 0
  end
end
