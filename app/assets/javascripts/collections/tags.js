QuiqNote.Collections.Tags = Backbone.Collection.extend({
  comparator: function(labelA, labelB) {
    if (labelA.get('label').toLowerCase() < labelB.get('label').toLowerCase()) return -1;
    if (labelB.get('label').toLowerCase() < labelA.get('label').toLowerCase()) return 1;
    return 0;
  },

  model: QuiqNote.Models.Tag,

  url: '/api/tags',

  getOrFetch: function(id) {
    var tags = this;
    var tag = this.get(id) ||
      new QuiqNote.Models.Tag({ id: id });
    tag.fetch({
      success: function() {
        tags.add(tag, { merge: true });
      }
    });
    return tag;
  }
});

QuiqNote.Collections.tags = new QuiqNote.Collections.Tags();
