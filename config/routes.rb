Rails.application.routes.draw do
  root 'static_pages#root'
  get 'index' => 'static_pages#index', defaults: { format: :html }
  get 'shared/:id' => 'shared_notes#shared', defaults: { format: :html }

  resource :user, only: [:new, :create]
  resource :session, only: [:new, :create, :destroy]

  namespace :api, defaults: { format: :json } do
    resources :notebooks, except: [:new, :edit]
    resources :notes, except: [:new, :edit] do
      get 'search', on: :collection
    end
    resources :tags, only: [:index, :show, :destroy] do
      get 'search', on: :collection
    end
  end
end
