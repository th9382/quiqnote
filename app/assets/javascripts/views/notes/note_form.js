QuiqNote.Views.NoteForm = Backbone.View.extend({
  template: JST['notes/form'],

  events: {
    "click button.save-form": "updateNote",
    "blur input[name='tags']": "recordTag",
    "click button.share-note": "shareNote",
    "click .help-button": "walkthrough"
  },

  shareNote: function(event) {
    event.preventDefault();
    var view = new QuiqNote.Views.ShareNote({
      model: this.model
    });
    view.render();
  },

  recordTag: function(event) {
    var $target = $(event.currentTarget);
    var e = $.Event("keydown");
    e.which = 13; // Enter
    $target.trigger(e);
  },

  initialize: function(options) {
    this.notebooks = options.notebooks;
    this.listenTo(this.notebooks, "sync", this.render);
    this.listenTo(this.model, "sync", this.render);
  },

  initEditor: function() {
      this.editor = new Quill('#full-editor', {
      modules: {
        'toolbar': { container: '#full-toolbar' },
        'image-tooltip': true,
        'link-tooltip': true,
      },
      theme: 'snow'
    });
    if (this.model.get('content_delta')) {
      this.editor.setContents(JSON.parse(this.model.get('content_delta')));
    }
  },

  render: function() {
    var content = this.template({
      note: this.model,
      notebooks: this.notebooks
    });

    this.$el.html(content);

    if ($('#full-editor').length > 0 ){
        this.initEditor();
        this.enableTags();

        // create dropdown
        if ($("#note_notebook_id").find("option").length > 0) {
          $("#note_notebook_id").dropkick();
        }
    }
    return this;
  }, enableTags: function () {
    // typeahead js
    var tags = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      remote: {
        url: '/api/tags/search/?query=%QUERY',
        filter: function(list) {
          return $.map(list, function(tag) { return { name: tag }; });
        }          }
    });

    tags.initialize();

    var currentTags = this.model.escape('tags').split(',') || [];

    jQuery(".tm-input").tagsManager({
      prefilled: currentTags,
      delimiters: [13],
      deleteTagsOnBackspace: false
    });

    jQuery(".tm-input.tm-input-typeahead").typeahead(null, {
      name: 'tags',
      displayKey: 'name',
      source: tags.ttAdapter()
    });

    jQuery(".tm-input").on('tm:refresh', function(e, taglist) {
      $("input[name='tags']").typeahead('val', '');
    });
  },

  updateNote: function(event) {
    event.preventDefault();
    var noteAction = this.model.id ? 'update' : 'new';
    this.model.set({
      title: $("#note_title").val(),
      notebook_id: $("#note_notebook_id").val(),
      content_delta: JSON.stringify(this.editor.getContents()),
      content_html: this.editor.getHTML(),
      content: this.editor.getText(),
      tags: $("input[name='hidden-tags']").val(),
      resetSharedToken: false
    });
    this.model.save({}, {
      success: function(model, response) {
        this.collection.add(model, { merge: true, at: 0 });
        var notifyMsg = 'Updated note';

        if (noteAction === 'new') { notifyMsg = 'Created new note'; }
        humane.log(notifyMsg, { timeout: 1900, clickToClose: true });
        window.history.back();
      }.bind(this),
      error: function(model, response) {
        // remove [ ] and quotes
        var msg = response.responseText.replace(/\[|\]|"/g, "");
        humane.error = humane.spawn({ addnCls: 'humane-libnotify-error', timeout: 1900 });
        humane.error(msg);
      }
    });
  },

  walkthrough: function() {
    $('#main').scrollTop(0);
    var helpGuide = introJs();
    helpGuide.setOptions({
      steps: [
          {
            intro: 'If you clicked on New Note in the sidebar, this page will show you a blank form to create a new note.<br><br> Otherwise, it will show you the content of an existing note.',
            position: 'left'
          },
          {
            element: '#note_title',
            intro: "Title your note by adding text in this field. Each note requires a title, otherwise you will get an error message.",
            position: 'bottom'
          },
          {
            element: '#full-toolbar',
            intro: "This toolbar gives you access to create rich formatting.",
            position: 'left'
          },
          {
            element: '#full-editor',
            intro: "The heart of the app. Type your notes here.",
            position: 'left'
          },
          {
            element: '.save-form',
            intro: "Once you're done editing your note, make sure to click this button, which will save the note.",
            position: 'left'
          },
          {
            element: '.notebook-icon',
            intro: "Optionally, you can specify which notebook to store the note in.<br><br>For a new note, this is pre-filled depending on which notebook you were in when you clicked on the New Note link",
            position: 'bottom'
          },
          {
            element: '.tags-icon',
            intro: "Optionally, add tags to your notes. Just click here and type the name of the tag.<br><br>If the tag already exists on one of your other notes, a helpful typeahead search feature will suggest the name of the tag. Otherwise, just add a new one.",
            position: 'right'
          },
          {
            element: '.share-note',
            intro: "Clicking this button will bring up a new screen where you will get a link that you can share. Anyone with that link can then view your note (but they can't edit anything).<br><br>Also click here to stop sharing the note.<br><br>If this is a new note, first click Done and come back to share.",
            position: 'left'
          }
      ]
    });
    helpGuide.start();
  }
});
