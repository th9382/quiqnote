require 'rails_helper'

# save_and_open_page

feature "User signin" do
  background do
    @user = User.create!(email: 'example@example.com', password: 'password');
  end
  scenario "Signing in with correct credentials" do
    page.visit "/session/new"
    page.fill_in "user[email]", with: 'example@example.com'
    page.fill_in "user[password]", with: 'password'
    page.click_button "Sign in"
    expect(page).to have_content("NOTES")
  end

  scenario "User tries to sign in with incorrect password" do
    page.visit "/session/new"
    page.fill_in "user[email]", with: 'example@example.com'
    page.fill_in "user[password]", with: 'wrongpassword'
    page.click_button "Sign in"
    expect(page).to have_content("Invalid")
  end
end

feature "User sign up" do
  scenario "Signing up correctly" do
    page.visit "/user/new"
    page.fill_in "user_email" , with: 'example2@example.com'
    page.fill_in "user_password", with: 'password'
    page.click_button "Sign up"
    expect(page).to have_content("NOTES")
  end

  scenario "User tries to sign up with wrong email" do
    page.visit "/user/new"
    page.fill_in "user_email" , with: 'example2example.com'
    page.fill_in "user_password", with: 'password'
    page.click_button "Sign up"
    expect(page).to have_content("Email is invalid")
  end

  scenario "User tries to sign up with short password" do
    page.visit "/user/new"
    page.fill_in "user_email" , with: 'example3@example.com'
    page.fill_in "user_password", with: 'pass'
    page.click_button "Sign up"
    expect(page).to have_content("Password is too short")
  end
end
