# QuiqNote

[Website link][weburl]

[weburl]: https://www.quiqnote.com/

## Concept

QuiqNote is a note-taking app (inspired by Evernote's web app) that allows users to create rich content (notes), organize those notes, and easily find notes via search, tags, shortcuts, and notebooks. The overall design is minimalistic with rich user feedback when navigating the site. Optimized for Chrome web browser.

This is my App Academy capstone project. Designed and built within a 2-week time period.

## Key Features
- Create notes and Notebooks
- Assign one or many tags to notes
- Search for notes
- Move notes to trash. From the trash, restore or delete notes permanently
- Create shortcuts for notes for quick access
- Share notes via a unique URL
- Compact list views
- Sortable Notebooks
- Easy to use - page walk-throughs
- Infinite scrolling for long lists

## Technologies
- Backbone js
- Rails
- Quill js (text editor)
- Typeahead js
- Tagmanager js (tags)
- Humane js (notifications)
- timeago js (slightly modified)
- Bootstrap (responsive design)
- jQuery UI sortable (sorting notebooks)
- Animate css
- Font Awesome (icons)
- Capybara / rspec (integration tests)
- Heroku / AWS
- kaminari gem (pagination)
- App Academy's CompositeView

## Minimum Viable Product
QuiqNote is built on Rails and Backbone. Users can:

- [x] Create accounts
- [x] Create sessions (log in)
- [x] Create Notebooks
- [x] Create Notes
- [x] View and edit notes
- [x] Notes can have rich formatting (bold, italics, underline, highlight)
- [x] Add tags to notes
- [x] Search for notes

## Design Docs
* [View Wireframes][views]
* [DB schema][schema]

[views]: ./docs/views.md
[schema]: ./docs/schema.md

## Implementation Timeline

### Phase 1: User Authentication Creation, basic setup (~0.5 days)
I will implement user authentication in Rails based on the practices learned at
App Academy. By the end of this phase, users will be able to create an account and log in via a simple form in a Rails view. The most important part of this phase will be pushing the app to Heroku and ensuring that everything works before moving onto phase 2. In this phase I will also set up helpful gems for the dev environment, such as annotate, better_errors, pry-rails, etc.

[Details][phase-one]

### Phase 2: Viewing and creating Notebooks and Notes (~1.5-2 days)
I will add the necessary Rails models for notebooks and notes. I will add API routes to serve notebooks and note data as JSON, then add Backbone models and collections that fetch data from those routes. By the end of this phase, users will be able to create and view notebooks and notes all inside a single Backbone app.

[Details][phase-two]

### Phase 3: Rich text editor for notes (~3 days)
I plan to use a third-party js library to provide rich text editing capabilities to the `NoteForm` view. I will need to make sure notes can be edited seamlessly and the text-data is properly escaped and stored in the database.

[Details][phase-three]

### Phase 4: Add tags to Notes (~1.5-2 days)

I will implement the tag system so a user can assign many tags to notes. I will start with creating the Rails models and update the Notes API (Rails controller) to support tags. I will likely use a third-party tagging library on the front-end but need to do further research here. At the end of this phase, a user should be able to seamlessly add many tags to a note.

[Details][phase-four]

### Phase 5: Search for Notes / Notebooks / Tags (~2-3 days)
I will add functionality to search for notes by tags or title. I will need to add `search` routes to the Notes controller in Rails.
On Backbone, I will have a `SearchResults` view with `NotesIndex` subviews.

[Details][phase-five]

### Bonus Features (TBD)

- [x] Modal confirm when deleting notebook / notes
- [x] Trash and restore
- [x] Shortcuts for Notes
- [x] Typeahead for tags
- [x] Compact view for notes list
- [x] Share notes
- [x] Sort notebooks
- [x] Site walkthrough
- [x] Integration tests
- [x] Infinite scroll
- [x] Responsive design
- [ ] Reminders for notes
- [ ] Persistent login
- [ ] Activity history (last edit, created notes, deleted notes)
- [ ] Summary of highlights in a note
- [ ] Markdown support

[phase-one]: ./docs/phases/phase1.md
[phase-two]: ./docs/phases/phase2.md
[phase-three]: ./docs/phases/phase3.md
[phase-four]: ./docs/phases/phase4.md
[phase-five]: ./docs/phases/phase5.md
