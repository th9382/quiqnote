# == Schema Information
#
# Table name: tags
#
#  id                 :integer          not null, primary key
#  label              :string           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  owner_id           :integer          not null
#  active_notes_count :integer          default(0)
#

class Tag < ActiveRecord::Base
  belongs_to :owner, foreign_key: :owner_id, class_name: 'User'
  has_many :taggings, class_name: 'Tagging', dependent: :destroy
  has_many(
    :notes,
    ->{ where( notes: { deleted: false }) },
    through: :taggings,
    source: :note
  )

  validates :label, :owner_id, presence: true

  def update_active_notes_count
    self.active_notes_count = notes.count
    self.save
  end

end
