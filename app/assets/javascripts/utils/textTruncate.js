
QuiqNote.textTruncate = function(text, numChars) {
  if (text.length > numChars) {
    var newText = text.slice(0, numChars);
    return newText += "...";
  } else {
    return text;
  }
};
