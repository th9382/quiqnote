class StaticPagesController < ApplicationController
  before_action :require_login, only: :root

  def index
    if current_user
      redirect_to root_url
    end
  end

  def root
  end
end
