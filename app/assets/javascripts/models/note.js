QuiqNote.Models.Note = Backbone.Model.extend({
  urlRoot: '/api/notes',
  
  modelType: 'Note'
});
