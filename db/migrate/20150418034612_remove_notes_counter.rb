class RemoveNotesCounter < ActiveRecord::Migration
  def change
    remove_column :notebooks, :notes_count
  end
end
