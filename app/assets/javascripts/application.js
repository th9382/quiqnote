// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui/sortable
//= require jquery.serializejson
//= require jquery.timeago
//= require typeahead.bundle
//= require bootstrap-sprockets
//= require underscore
//= require tagmanager
//= require backbone
//= require humane
//= require dropkick
//= require quiq_note
//= require quill
//= require intro
//= require_tree ../templates
//= require_tree ./models
//= require_tree ./collections
//= require_tree ./utils
//= require_tree ./views/base
//= require_tree ./views/layout
//= require_tree ./views/notes
//= require_tree ./views/notebooks
//= require_tree ./views/tags
//= require_tree ./views/
//= require_tree ./routers
//= require_tree .

// write bind method for poltergeist in test
Function.prototype.bind = function(context) {
  var fn = this;
  var bindArgs = Array.prototype.slice.call(arguments, 1);
  return function() {
    var callArgs = Array.prototype.slice.call(arguments);
    return fn.apply(context, bindArgs.concat(callArgs));
  };
};
