class AddSharedBolleanToNotes < ActiveRecord::Migration
  def change
    add_column :notes, :shared, :boolean, default: false
  end
end
