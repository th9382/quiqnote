class SharedNotesController < ApplicationController
  def shared
    @note = Note.find_by_shared_token(params[:id])
    if @note && params[:id] && @note.shared
      render :show
    else
      redirect_to index_url
    end
  end
end
