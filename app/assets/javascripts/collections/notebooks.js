QuiqNote.Collections.Notebooks = Backbone.Collection.extend({
  comparator: function(notebook) {
    return notebook.get('ord');
  },

  model: QuiqNote.Models.Notebook,

  url: '/api/notebooks',

  getOrFetch: function(id) {
    var notebooks = this;
    var notebook = this.get(id) ||
      new QuiqNote.Models.Notebook({ id: id });
    notebook.fetch({
      success: function() {
        notebooks.add(notebook, { merge: true });
      }
    });
    return notebook;
  }
});


QuiqNote.Collections.notebooks = new QuiqNote.Collections.Notebooks();
