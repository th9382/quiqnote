# View Wireframes

## Landing Page
![landing-page]

## New Session
![new-session]

## New User
![new-user]

## Note Index
![note-index]

## Note Show and Form
![note-show-form]

## Notebook Index
![notebook-index]

## Notebook Show
![notebook-show]

## Tag Index
![tag-index]

## Note Search Results
![note-search]

## Sidebar
![sidebar]


[landing-page]: ./wireframes/landing_page.png
[new-session]: ./wireframes/new_session.png
[new-user]: ./wireframes/new_user.png
[note-index]: ./wireframes/note_index.png
[note-show-form]: ./wireframes/note_show_form.png
[notebook-index]: ./wireframes/notebook_index.png
[notebook-show]: ./wireframes/notebook_show.png
[tag-index]: ./wireframes/tag_index.png
[note-search]: ./wireframes/note_search_results.png
[sidebar]: ./wireframes/sidebar.png
