QuiqNote.Routers.Router = Backbone.Router.extend({
  initialize: function(options) {
    this.$rootEl = options.$rootEl;
    this._notebooks = QuiqNote.Collections.notebooks;
    this._notes = QuiqNote.Collections.notes;
    this._tags = QuiqNote.Collections.tags;
  },

  routes: {
    "": "notesIndex",
    "notes": "notesIndex",
    "notebooks": "notebooksIndex",
    "trash": "trashShow",
    "tags": "tagsIndex",
    "shortcuts": "shortcutShow",
    "search": "notesSearch",
    "tags/:id": "tagShow",
    "notebooks/new": "notebookNew",
    "notebooks/:id": "notebookShow",
    "notes/new": "noteNew",
    "notes/:id": "noteShow"
  },

  // Special notebooks
  shortcutShow: function() {
    this._notes.reset();
    this._notes.page = 1;
    this._notes.fetch({ data: { page: 1, pageType: 'shortcuts' }, processData: true });
    var view = new QuiqNote.Views.Shortcuts({ collection: this._notes });
    this._swapView(view);
  },

  trashShow: function() {
    this._notes.reset();
    this._notes.page = 1;
    this._notes.fetch({ data: { page: 1, pageType: 'trash'}, processData: true });
    var view = new QuiqNote.Views.Trash({ collection: this._notes });
    this._swapView(view);
  },

  notesSearch: function() {
    var view = new QuiqNote.Views.NotesSearch();
    this._swapView(view);
    view.$("#search-query").focus();
  },

// Tags
  tagsIndex: function() {
    this._tags.reset();
    this._tags.fetch();
    var view = new QuiqNote.Views.TagsIndex({ collection: this._tags });
    this._swapView(view);
  },

  tagShow: function(id) {
    this._notes.reset();
    this._notes.fetch({
      data: {
        page: 1,
        pageType: 'tags',
        tag_id: id
      },
      processData: true });

    var tag = this._tags.getOrFetch(id);
    var view = new QuiqNote.Views.TagShow({
      model: tag,
      collection: this._notes });
    this._swapView(view);
  },

  // Notes
  noteShow: function(id) {
    this._notebooks.fetch();
    var note = this._notes.getOrFetch(id);
    var view = new QuiqNote.Views.NoteForm({
      model: note,
      collection: this._notes,
      notebooks: this._notebooks
    });
    this._swapView(view);
  },

  noteNew: function() {
    this._notes.fetch({
      remove: true,
      data: { page: 1 }
    });
    this._notebooks.fetch();
    var note = new QuiqNote.Models.Note();
    var view = new QuiqNote.Views.NoteForm({
      collection: this._notes,
      model: note,
      notebooks: this._notebooks
    });
    this._swapView(view);
  },

  notesIndex: function() {
    this._notes.reset();
    this._notes.page = 1;
    this._notes.fetch({
      remove: true,
      data: { page: 1 }
    });
    var view = new QuiqNote.Views.NotesIndex({
      collection: this._notes,
      infiniteScroll: true
     });

    this._swapView(view);
  },

  // Notebooks
  notebooksIndex: function() {
    this._notebooks.reset();
    this._notebooks.fetch();
    var view = new QuiqNote.Views.NotebooksIndex({
      collection: this._notebooks
    });
    this._swapView(view);
  },

  notebookNew: function() {
    this._notebooks.fetch();
    var notebook = new QuiqNote.Models.Notebook();
    var view = new QuiqNote.Views.NotebookForm({
      collection: this._notebooks,
      model: notebook
    });
    this._swapView(view);
    view.$("input:text:visible:first").focus();
  },

  notebookShow: function(id) {
    this._notes.reset();
    this._notes.fetch({
      data: {
        page: 1,
        pageType: 'notebook',
        notebook_id: id
      },
      processData: true });

    var notebook = this._notebooks.getOrFetch(id);
    var view = new QuiqNote.Views.NotebookShow({
      model: notebook,
      collection: this._notes
    });
    this._swapView(view);
  },

  // utils
  _swapView: function(view) {
    this.setPrevView();

    this._currentView && this._currentView.remove();
    this._currentView = view;
    this.$rootEl.html(view.render().$el);
  },

  setPrevView: function() {
    if (this._currentView && this._currentView.model) {
      QuiqNote.previousView = {
        view: this._currentView,
        modelType: this._currentView.model.modelType,
        modelId: this._currentView.model.id,
      };
    }
  }
});
