class SessionsController < ApplicationController
  before_action :require_login, only: :destroy

  def new
    @user = User.new
  end

  def create
    @user = User.find_by_credentials(
      params[:user][:email],
      params[:user][:password]
    )
    if @user
      login_user!(@user)
      redirect_to root_url
    else
      @user = User.new
      flash.now[:errors] = ['Invalid email or password']
      render :new
    end
  end

  def destroy
    logout_user!
    redirect_to index_url
  end
end
