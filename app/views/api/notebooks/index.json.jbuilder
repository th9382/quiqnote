json.array! @notebooks do |notebook|
  json.extract!(
    notebook,
    :id,
    :title,
    :ord,
    :created_at,
    :updated_at,
    :active_notes_count
  )
  json.notes notebook.notes do |note|
    json.extract! note, :id, :title, :deleted, :shortcut
  end
end
