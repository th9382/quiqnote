QuiqNote.Views.NotesIndexBase = Backbone.CompositeView.extend({
  template: JST['notes/index'],

  className: 'notes__container',

  initialize: function(options) {
    $("#main").scrollTop(0);
    this.listenTo(this.collection, "sync", this.render);
    this.listenTo(this.collection, 'add', this.addIndexItem);
    this.listenTo(this.collection, 'remove', this.removeIndexItem);

    this.headerText = 'notes';

    this.infiniteScroll = true;
    this.fetchingData = false;

    this._subviews = {};

    this.collection.each(this.addIndexItem.bind(this));

    $("#main").off('scroll');

    if (this.infiniteScroll) {
      // bind for infinite scroll functionality
      _.bindAll(this, 'detect_scroll');
      $("#main").on('scroll', this.detect_scroll);
    }
  },

  dataAttribs: function() {},

  events: {
    "click a.compactLink": "compactView",
    "click .help-button": "walkthrough",
  },

  addIndexItem: function (note) {
    var subview = new QuiqNote.Views.NotesIndexItem({
      model: note,
      collection: this.collection,
      pageType: this.pageType
    });

    this.addSubview('.notes__list', subview);
  },

  removeIndexItem: function (note) {
    this.removeModelSubview('.notes__list', note);
    this.updateNotesCount(-1);
  },

  updateNotesCount: function(n) {
    this.collection.notes_count += n;
    var count = this.collection.notes_count;
    var string = QuiqNote.pluralizeNote(count);
    this.$(".notes-counter").html(count + " " + string);
  },

  render: function(model, attr, options) {
    if (options && options.renderFalse) {
      return;
    }
    this.$el.html(this.template({
      notes: this.collection,
      headerText: this.headerText,
      pageType: this.pageType
    }));

    this.attachSubviews();

    setTimeout(function() { this.$("abbr.timeago").timeago(); }, 1);
    return this;
  },

  detect_scroll: function(event) {
    if (this.fetchingData ||
          this.collection.page >= this.collection.total_pages ||
          !this.infiniteScroll ) {
      return;
    }

    var buffer = 0.8; // set how early it should fetch
    var scrollTop = $('#main').scrollTop();
    var documentHeight = $(document).height();
    var listHeight = $('.notes__list').height();

    if((scrollTop + documentHeight) > (listHeight * buffer)) {
      this.fetchingData = true;
      this.collection.fetch({
        remove: false,
        data: this.dataAttribs(),
        processData: true,
        success: function() {
          this.fetchingData = false;
        }.bind(this)
      });
    }
  },

  compactView: function(event) {
    event.preventDefault();
    $target = $(event.currentTarget);
    QuiqNote.compactView = !QuiqNote.compactView;

    this.$(".notes__list__item").toggleClass("compact");
    this.$(".notes__list__item__container__content").toggleClass("compact");
    if (QuiqNote.compactView) {
      $target.text("Expand view");
    } else {
      $target.text("Compact view");
    }
  },

  walkthrough: function() {}
});
