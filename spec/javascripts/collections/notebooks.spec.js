describe("QuiqNote.Collections.Notebooks collection", function () {
  beforeEach(function () {
    this.notebooks = new QuiqNote.Collections.Notebooks();
  });

  it("has the correct model defined", function () {
    expect(this.notebooks.model).toEqual(QuiqNote.Models.Notebook);
  });

  it("has the correct url defined", function () {
    expect(this.notebooks.url).toEqual('/api/notebooks');
  });
});
