# Phase 4: Add tags to Notes

## Rails
### Models
* tags
* taggings

### Controllers
* Api::TagsController

### Views
* tags/show.json.jbuilder

## Backbone
### Models

### Collections

### Views
* TagsIndex (composite view, contains TagsIndexItem subview)
* TagsIndexItem

## Gems/Libraries
* tagging library (tagmanager)
