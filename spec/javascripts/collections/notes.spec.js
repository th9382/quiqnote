describe("QuiqNote.Collections.Notes collection", function () {
  beforeEach(function () {
    this.notes = new QuiqNote.Collections.Notes();
  });

  it("has the correct model defined", function () {
    expect(this.notes.model).toEqual(QuiqNote.Models.Notes);
  });

  it("has the correct url defined", function () {
    expect(this.notes.url).toEqual('/api/notes');
  });

  // TODO Use helper to create these?

  // it("can be sorted by order (comparator function)", function () {
  //   var notebook1 = new QuiqNote.Models.Notes({ order: 3 });
  //   var notebook2 = new QuiqNote.Models.Notes({ order: 4 });
  //   var notebook3 = new QuiqNote.Models.Notes({ order: 1 });
  //
  //   this.notes.add([notebook1, notebook2, notebook3]);
  //   expect(this.notes.sort().first()).toEqual(post3);
  //   expect(this.notes.sort().last()).toEqual(post2);
  // });
});
