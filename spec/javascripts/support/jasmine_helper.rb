#Use this file to set/override Jasmine configuration options
#You can remove it if you don't need it.
#This file is loaded *after* jasmine.yml is interpreted.
#
#Example: using a different boot file.
#Jasmine.configure do |config|
#   config.boot_dir = '/absolute/path/to/boot_dir'
#   config.boot_files = lambda { ['/absolute/path/to/boot_dir/file.js'] }
#end
#
#Example: prevent PhantomJS auto install, uses PhantomJS already on your path.
#Jasmine.configure do |config|
#   config.prevent_phantom_js_auto_install = true
#end
#


# def make_one_note(title, content, notebook)
#   visit "#/posts/new"
#   fill_in "Title", with: title
#   fill_in "Body", with: body
#   click_button "Submit"
# end
#
# def edit_post(post, title)
#   visit "#/posts/#{post.id}/edit"
#   fill_in "Title", with: title
#   click_button "Submit"
# end
