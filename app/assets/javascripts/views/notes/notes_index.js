QuiqNote.Views.NotesIndex = QuiqNote.Views.NotesIndexBase.extend({
  initialize: function() {
    QuiqNote.Views.NotesIndexBase.prototype.initialize.call(this);
  },

  dataAttribs: function() {
    return {
      page: ++this.collection.page,
    };
  },

  walkthrough:function() {
    $('#main').scrollTop(0);
    var helpGuide = introJs();
    helpGuide.setOptions({
      steps: [
          {
            intro: 'This guided tour will explain the main features of QuiqNote.<br><br>Use the arrow keys for navigation or hit ESC to exit the tour immediately.',
            position: 'top'
          },
          {
            element: '.help-button',
            intro: 'Every page that has a walk-through will have this link. Click it to get a quick tour of the page.',
            position: 'top'
          },
          {
            element: '.notes__body',
            intro: "This is a list of all your notes. <br><br>It's sorted by the last time you updated the note.<br><br>Each note shows the last time it was updated, the title, and a summary of the content if you're not in the compact view.",
            position: 'left'
          },
          {
            element: '.compactLink',
            intro: "Clicking this will toggle a compact view, which shows more items in the list and hides the content summary. ",
            position: 'right',
            scrollToElement: true
          },
          {
            element: '.notes__list__item:first-child',
            intro: "You can click on each note, which brings you to a new page where you can see all the content and edit it. When you hover over a note, you are presented with 2 options: <br><br>1) Shortcut, which creates (or removes) a shortcut for that note so it shows up in your Shortcuts list, <br><br>2) Trash, which moves the note the trash.",
            position: 'left'
          },
          {
            element: '#sidebar',
            intro: 'This sidebar allows you to easily navigate the site. <br><br>If you have a small browser window, it will shrink to a compact view.',
            position: 'right'
          },
          {
            element: '.sidebar__list__item--new-note',
            intro: 'Clicking this link will bring you to a page where you can create a new note.',
            position: 'right'
          },
          {
            element: '.sidebar__list__item--search',
            intro: "Navigate to the search page which lets you easily find the note you're looking for by title, tags, or content.",
            position: 'right'
          },
          {
            element: '.sidebar__list__item--notes',
            intro: "Shows you a list of all your notes. This is also the default page when you sign in.",
            position: 'right'
          },
          {
            element: '.sidebar__list__item--notebooks',
            intro: "Shows you a list of all your notebooks.",
            position: 'right'
          },
          {
            element: '.sidebar__list__item--shortcuts',
            intro: "Shows you a list of all your notes for which you created a shortcut.",
            position: 'right'
          },
          {
            element: '.sidebar__list__item--tags',
            intro: "Shows you a list of all the tags that are tied to your notes.",
            position: 'right'
          },
      ]
    });
    helpGuide.start();
  }
});
