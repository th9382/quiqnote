class RemoveContentDeltaFromNotes < ActiveRecord::Migration
  def change
    remove_column :notes, :content_delta
  end
end
