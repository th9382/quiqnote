QuiqNote.Views.NotebooksIndexItem = Backbone.View.extend({
  template: JST['notebooks/index_item'],

  className: 'row notebook__list__item',

  tagName: 'li',

  events: {
    "click .actions__img--trash": "spawnModal"
  },
  initialize: function() {
    this.model && this.$el.attr("data-id", this.model.id);
  },

  render: function() {
    var content = this.template({ notebook: this.model });
    this.$el.html(content);
    return this;
  },

  spawnModal: function(event) {
    event.preventDefault();
    var modal = new QuiqNote.Views.Modal({
      model: this.model,
      collection: this.collection
    });

    modal.render();
  }
});
