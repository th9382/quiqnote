class AddNotesCountToTags < ActiveRecord::Migration
  def change
    add_column :tags, :active_notes_count, :integer, default: 0
  end
end
