QuiqNote.Views.NotesIndexItem = Backbone.View.extend({
  template: JST['notes/index_item'],

  className: function() {
    return QuiqNote.compactView ? 'row notes__list__item compact' :
      'row notes__list__item';
  },

  tagName: 'li',

  events: {
    "click .actions__img--trash, .note__erase": "spawnModal",
    "click .actions__img--shortcut":            "updateShortcut",
    "click .actions__img--noshortcut":          "updateShortcut",
    "click .note__restore":                     "restoreModel"
  },

  initialize: function(options) {
    this.listenTo(this.model, "sync", this.render);
    this.pageType = options.pageType;
  },

  render: function(model, attr, options) {
    var content = this.template({ note: this.model });
    this.$el.html(content);
    return this;
  },

  restoreModel: function(event) {
    event.preventDefault();
    this.model.save({ deleted: false },{
      patch: true,
      success: function() {
        humane.log('Restored ' + this.model.escape('title'), { timeout: 1900, clickToClose: true });
        if (this.pageType === 'trash') {
          this.collection.remove(this.model, { trigger: true });
        }
      }.bind(this)
    });
    this.remove();
  },

  spawnModal: function(event) {
    event.preventDefault();
    var modal = new QuiqNote.Views.Modal({
      model: this.model,
      collection: this.collection
    });
    modal.render();
  },

  updateShortcut: function(event) {
    event.stopPropagation();
    var newVal = !this.model.get("shortcut");

    this.model.save({ shortcut: newVal }, {
      renderFalse: true,
      success: function() {
        var msg = newVal ? 'Created shortcut' : 'Removed shortcut';
        humane.log(msg, { timeout: 1900, clickToClose: true });

        if (this.pageType === 'shortcuts') {
          this.collection.remove(this.model, { trigger: true });
        }
        setTimeout(function() { this.$("abbr.timeago").timeago(); }, 1);
      }.bind(this)
    });

  }
});
