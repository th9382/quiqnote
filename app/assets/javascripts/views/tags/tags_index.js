QuiqNote.Views.TagsIndex = Backbone.CompositeView.extend({
  template: JST['tags/index'],

  className: 'col-xs-10 col-xs-offset-1 notebook__body',

  events: {
    "click .help-button": "walkthrough"
  },

  initialize: function(options) {
    this.listenTo(this.collection, "sync", this.render);
    this.listenTo(this.collection, "add", this.addIndexItem);
    this.listenTo(this.collection, "remove", this.removeIndexItem);

    this._subviews = {};

    this.collection.each(this.addIndexItem.bind(this));
  },

  addIndexItem: function(tag) {
    var subview = new QuiqNote.Views.TagsIndexItem({
      model: tag,
      collection: this.collection
    });

    this.addSubview(".notebook__list", subview);
  },

  removeIndexItem: function(tag) {
    this.removeModelSubview(".notebook__list", tag);
  },

  render: function() {
    this.$el.html(this.template());

    this.attachSubviews();

    if ($(".notebook__list").children().length === 0) {
      var $noTags = $("<li class='noTags-text'>You have no tags</li>");
      $(".notebook__list").append($noTags);
    }

    return this;
  },

  walkthrough: function() {
    $('#main').scrollTop(0);
    var helpGuide = introJs();
    helpGuide.setOptions({
      steps: [
          {
            element: '.notebook__list',
            intro: 'This page shows all the tags that are assigned to your notes.',
            position: 'left'
          },
          {
            element: '.notebook__list__item:first-child',
            intro: "Clicking on a tag will show you a new view, with a list of all the notes associated to that tag.<br><br> If you delete a tag, the tag will be removed from all the notes. However, the notes will not be deleted!.",
            position: 'left'
          },
      ]
    });
    helpGuide.start();
  }
});
