QuiqNote.Views.Trash = Backbone.CompositeView.extend({
  template: JST['notebooks/trash'],

  className: 'notebook__body',

  initialize: function(options) {
    this.listenTo(this.collection, "sync remove", this.render);
    this.notesIndexView = new QuiqNote.Views.TrashNotesIndex({
      collection: this.collection,
    });

    this._subviews = {};

    this.addSubview('.notebook__notes', this.notesIndexView);
  },

  render: function() {
    this.$el.html(this.template());

    this.attachSubviews();
    this.$("abbr.timeago").timeago();
    return this;
  }
});
